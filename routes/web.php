<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/login/admin', 'AdminController@showLogin')->name('adminLogin');
Route::post('/login/admin', 'AdminController@login')->name('adminLogin');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/category', 'CategoryController@index')->name('category.index');
Route::get('/category/api/fetch', 'CategoryController@fetch')->name('category.fetch');
Route::get('/category/create', 'CategoryController@create')->name('category.create');
Route::post('/category/api/store', 'CategoryController@store')->name('category.store');
Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
Route::post('/category/api/update/{id}', 'CategoryController@update')->name('category.update');
Route::post('/category/api/delete/{id}', 'CategoryController@destroy')->name('category.delete');





Route::get('/subcategory/manage', 'SubcategoryController@index')->name('subcategory.index');
Route::get('/subcategory/api/fetch', 'SubcategoryController@fetch')->name('subcategory.fetch');
Route::get('/subcategory/create', 'SubcategoryController@create')->name('subcategory.create');
Route::post('/subcategory/api/store', 'SubcategoryController@store')->name('subcategory.store');
Route::get('/subcategory/edit/{id}', 'SubcategoryController@edit')->name('subcategory.edit');
Route::post('/subcategory/api/update/{id}', 'SubcategoryController@update')->name('subcategory.update');
Route::post('/subcategory/api/delete/{id}', 'SubcategoryController@destroy')->name('subcategory.delete');





Route::get('/products/manage', 'ProductsController@index')->name('products.index');
Route::get('/products/api/fetch', 'ProductsController@fetch')->name('products.fetch');
Route::get('/products/create', 'ProductsController@create')->name('products.create');
Route::post('/products/api/store', 'ProductsController@store')->name('products.store');
Route::get('/products/edit/{id}', 'ProductsController@edit')->name('products.edit');
Route::post('/products/api/update/{id}', 'ProductsController@update')->name('products.update');
Route::post('/products/api/delete/{id}', 'ProductsController@destroy')->name('products.delete');
