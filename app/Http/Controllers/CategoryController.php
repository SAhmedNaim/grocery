<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Exception;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('category.manage');
    }

    public function create()
    {
        return view('category.create');
    }

    public function fetch()
    {
        $categories = Category::select('id', 'name', 'description', 'image')->get();

        return $categories;
    }

    public function store(Request $request)
    {
        try {
            $category = new Category;
            $category->name = $request->input('name');
            $category->description = $request->input('description');
            $category->image = $request->file('image')->getClientOriginalName();
            $category->save();

            $file = $request->file('image');
            $success = $file->move('uploads', $file->getClientOriginalName());
        } catch (Exception $exception) {
            $success = false;
        }

        return response()->json(['success' => (bool) $success]);
    }

    public function show()
    {
        // 
    }

    public function edit($id)
    {
        $category = Category::find($id);

        return view('category.update', compact('category'));
    }

    public function update($id, Request $request)
    {
        try{
            $category = Category::where('id', $id)->first();
            $category->name = $request->input('name');
            $category->description = $request->input('description');

            if(!empty($request->file('image'))) {
                $category->image = $request->file('image')->getClientOriginalName();

                $file = $request->file('image');
                $file->move('uploads', $file->getClientOriginalName());
            }

            $success = $category->save();
        } catch (Exception $exception) {
            $success = false;
        }

        return response()->json(['success' => (bool) $success]);
    }

    public function destroy($id)
    {
        try {
            $category = Category::where('id', $id)->first();
            $success = $category->delete();
        } catch(Exception $exception) {
            $success = false;
        }

        return response()->json(['success' => $success]);
    }
}
