@extends('layout.app')

@section('content')
   <subcategory-create :categories="{{ $categories }}"></subcategory-create>
@endsection