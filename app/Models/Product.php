<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Models\Category')->select('id', 'name');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Models\Subcategory')->select('id', 'name');
    }

}
