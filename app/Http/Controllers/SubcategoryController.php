<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Subcategory;
use Exception;

class SubcategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        return view('subcategory.manage');
    }

    public function create()
    {
        $categories = Category::select('id', 'name')->get();

        return view('subcategory.create', compact('categories'));
    }
    
    public function fetch()
    {
        $subcategories = Subcategory::select('id', 'name', 'description', 'image')->get();

        return $subcategories;
    }

    public function destroy($id)
    {
        try {
            $subcategory = Subcategory::where('id', $id)->first();
            $success = $subcategory->delete();
        } catch(Exception $exception) {
            $success = false;
        }

        return response()->json(['success' => $success]);
    }

    public function store(Request $request)
    {
        try {
            $subcategory = new Subcategory;
            $subcategory->name = $request->input('name');
            $subcategory->category_id = $request->input('category_id');
            $subcategory->description = $request->input('description');
            $subcategory->image = $request->file('image')->getClientOriginalName();
            $subcategory->save();

            $file = $request->file('image');
            $success = $file->move('uploads', $file->getClientOriginalName());
        } catch (Exception $exception) {
            $success = false;
        }

        return response()->json(['success' => (bool) $success]);
    }

    public function edit($id)
    {
        $categories = Category::select('id', 'name')->get();
        $subcategory = Subcategory::find($id);

        return view('subcategory.update', compact('categories', 'subcategory'));
    }

    public function update($id, Request $request)
    {
        try{
            $subcategory = Subcategory::where('id', $id)->first();
            $subcategory->name = $request->input('name');
            $subcategory->category_id = $request->input('category_id');
            $subcategory->description = $request->input('description');

            if(!empty($request->file('image'))) {
                $subcategory->image = $request->file('image')->getClientOriginalName();

                $file = $request->file('image');
                $file->move('uploads', $file->getClientOriginalName());
            }

            $success = $subcategory->save();
        } catch (Exception $exception) {
            $success = false;
        }

        return response()->json(['success' => (bool) $success]);
    }

 
  
   
}
