@extends('layout.app')

@section('content')
   <subcategory-update :categories="{{ $categories }}" :subcategory="{{ $subcategory }}"></subcategory-updates>
@endsection