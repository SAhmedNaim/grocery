<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Subcategory;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('products.manage');
    }

    public function create()
    {
        $categories = Category::select('id', 'name')->get();
        $subcategories = Subcategory::select('id', 'name')->get();

        return view('products.create', compact('categories', 'subcategories'));
    }

    public function fetch()
    {
        $products = Product::with('category', 'subcategory')
                ->select('id', 'category_id', 'subcategory_id', 'name', 'qty', 'unit_price', 'discount', 'short_description', 'long_description', 'image')
                ->get();

        return $products;
    }

    public function store(Request $request)
    {
        try{
            $product = new Product;
            $product->name = $request->input('name');
            $product->short_description = $request->input('short_description');
            $product->long_description = $request->input('long_description');
            $product->qty = $request->input('qty');
            $product->unit_price = $request->input('unit_price');
            $product->total_price = $request->input('total_price');
            $product->discount = $request->input('discount');
            $product->category_id = $request->input('category');
            $product->subcategory_id = $request->input('subcategory');

            if(!empty($request->file('image'))) {
                $product->image = $request->file('image')->getClientOriginalName();

                $file = $request->file('image');
                $file->move('uploads', $file->getClientOriginalName());
            }

            $success = $product->save();
        } catch (Exception $exception) {
            $success = false;
        }

        return response()->json(['success' => (bool) $success]);
    }

    public function show()
    {
        // 
    }

    public function edit($id)
    {
        $product = Product::with('category', 'subcategory')
                ->where('id', $id)
                ->select('id', 'category_id', 'subcategory_id', 'name', 'qty', 'unit_price', 'discount', 'short_description', 'total_price', 'long_description', 'image')
                ->first();

        $categories = Category::select('id', 'name')->get();
        $subcategories = Subcategory::select('id', 'name')->get();

        return view('products.update', compact('product', 'categories', 'subcategories'));
    }

    public function update(Request $request, $id)
    {
        try{
            $product = Product::find($id);
            $product->name = $request->input('name');
            $product->short_description = $request->input('short_description');
            $product->long_description = $request->input('long_description');
            $product->qty = $request->input('qty');
            $product->unit_price = $request->input('unit_price');
            $product->total_price = $request->input('total_price');
            $product->discount = $request->input('discount');
            $product->category_id = $request->input('category');
            $product->subcategory_id = $request->input('subcategory');

            if(!empty($request->file('image'))) {
                $product->image = $request->file('image')->getClientOriginalName();

                $file = $request->file('image');
                $file->move('uploads', $file->getClientOriginalName());
            }

            $success = $product->save();
        } catch (Exception $exception) {
            $success = false;
        }

        return response()->json(['success' => (bool) $success]);
    }

    public function destroy($id)
    {
        try {
            $product = Product::where('id', $id)->first();
            $success = $product->delete();
        } catch(Exception $exception) {
            $success = false;
        }

        return response()->json(['success' => $success]); 
    }
}
