@extends('layout.app')

@section('content')
   <products-create :categories="{{ $categories }}" :subcategories="{{ $subcategories }}"></products-create>       
@endsection