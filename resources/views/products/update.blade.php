@extends('layout.app')

@section('content')
   <products-update :product="{{ $product }}" :categories="{{ $categories }}" :subcategories="{{ $subcategories }}"></products-update>
@endsection