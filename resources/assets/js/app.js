/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

Vue.component('category-create', require('./components/category/create.vue'));
Vue.component('category-manage', require('./components/category/manage.vue'));
Vue.component('category-update', require('./components/category/update.vue'));


Vue.component('subcategory-create', require('./components/subcategory/create.vue'));
Vue.component('subcategory-manage', require('./components/subcategory/manage.vue'));
Vue.component('subcategory-update', require('./components/subcategory/update.vue'));



Vue.component('products-create', require('./components/products/create.vue'));
Vue.component('products-manage', require('./components/products/manage.vue'));
Vue.component('products-update', require('./components/products/update.vue'));






const app = new Vue({
    el: '#app'
});
